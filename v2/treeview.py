import pandas
import os

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import matplotlib as mpl

mpl.use('Agg')
import matplotlib.pyplot as plt

from ventana_generar_grafico import GraphOptions


class MainWindow(Gtk.Window):
	def __init__(self):
		super().__init__()
		self.set_default_size(900, 900)
		self.set_title("Colum")
		self.connect("destroy", Gtk.main_quit)

		self.grid = Gtk.Grid()
		self.grid.set_row_spacing(6)
		#self.grid.set_row_homogeneous(True)
		self.add(self.grid)

		self.create_headerbar()


	def create_headerbar(self):
		self.headerbar = Gtk.HeaderBar()
		self.headerbar.set_title("HeaderBar Example")
		#self.headerbar.set_subtitle("HeaderBar Subtitle")
		self.headerbar.set_show_close_button(True)
		self.set_titlebar(self.headerbar)

		self.add_app_buttons()


	def add_app_buttons(self):
		open_file_button = Gtk.Button()
		open_file_button.set_label("Abrir")
		open_file_button.connect("clicked", self.open_filechooser)
		self.headerbar.pack_start(open_file_button)

		new_graph_button = Gtk.Button()
		new_graph_button.set_label("Crear Grafico")
		new_graph_button.connect("clicked", self.graph_options)
		self.headerbar.pack_start(new_graph_button)


	def open_filechooser(self, widget=None):
		dialog = Gtk.FileChooserDialog(
									   title="Elija un archivo para abrir",
									   parent=self,
									   action=Gtk.FileChooserAction.OPEN
		)
		dialog.add_buttons(
			Gtk.STOCK_CANCEL,
			Gtk.ResponseType.CANCEL,
			Gtk.STOCK_OPEN,
			Gtk.ResponseType.OK,
		)

		self.add_filters_open(dialog)

		response = dialog.run()

		if response == Gtk.ResponseType.OK:
			self.create_treeview(dialog.get_filename())

		dialog.destroy()


	def add_filters_open(self, dialog):
		filter_csv = Gtk.FileFilter()
		filter_csv.set_name("csv")
		filter_csv.add_pattern("*.csv")
		dialog.add_filter(filter_csv)


	def graph_options(self, widget=None):
		# The app tries to create a GraphOptions window.
		try:
			opc = GraphOptions(self.data)

		# If there is no data, an exception is raised.
		except AttributeError:
			dialog = Gtk.MessageDialog(
									   parent=self,
									   flags=0,
									   message_type=Gtk.MessageType.WARNING,
									   buttons=Gtk.ButtonsType.CLOSE,
									   text="Imposible crear gráfico sin data"
									   )
			dialog.format_secondary_text("Cargue un archivo e intente de nuevo")
			dialog.run()
			dialog.destroy()

		# Else statement runs at the end of try statement only if no exceptions occur.
		else:
			opc.show_all()

			while True:
				response = opc.run()
				flag = 0

				if response == Gtk.ResponseType.OK:
					x = opc.get_x_axis()
					y = opc.get_y_axis()
					distinctive = opc.get_distinctive()
					colormap = opc.get_colormap()
					if x == "-Seleccione un parametro-" or y == "-Seleccione un parametro-":
						opc.raise_warning()
					else:
						flag = 1
				else:
					opc.destroy()
					break
				if flag == 1:
					opc.destroy()
					self.create_image(x, y, distinctive, colormap)
					break


	def create_treeview(self, path):
		scroll = Gtk.ScrolledWindow()
		scroll.set_hexpand(True)
		scroll.set_vexpand(True)
		self.grid.attach(scroll, 0, 1 , 1, 1)
		self.treeview = Gtk.TreeView()
		scroll.add(self.treeview)

		self.data = pandas.read_csv(path)

		if self.treeview.get_columns():
			for column in self.treeview.get_columns():
				self.treeview.remove_column(column)

		len_columns = len(self.data.columns)
		model = Gtk.ListStore(*(len_columns * [str]))
		self.treeview.set_model(model=model)

		cell = Gtk.CellRendererText()

		for item in range(len_columns):
			column = Gtk.TreeViewColumn(self.data.columns[item], cell, text=item)
			self.treeview.append_column(column)
			column.set_sort_column_id(item)

		for item in self.data.values:
			line = [str(x) for x in item]
			model.append(line)

		self.grid.remove_column(1)
		self.show_all()


	def create_image(self, x_axis, y_axis, distinctive, colormap='winter'):
		# clf() clears the figure, cla() clears the axes
		plt.clf()
		plt.cla()
		self.data['masc'] = self.data['Sexo'] == 'M'
		plt.scatter(self.data[x_axis], self.data[y_axis], c=self.data['masc'], cmap=colormap)
		plt.xlabel(x_axis)
		plt.ylabel(y_axis)
		plt.savefig('./graph.png')

		#scroll = Gtk.ScrolledWindow()
		#scroll.set_hexpand(True)
		#scroll.set_vexpand(True)

		self.image = Gtk.Image.new_from_file("./graph.png")
		self.grid.remove_column(1)
		self.grid.attach(self.image, 1, 1, 1, 1)
		#scroll.add(self.image)
		self.show_all()


def main():
	win = MainWindow()
	win.show_all()
	Gtk.main()


if __name__ == "__main__":
	main()
