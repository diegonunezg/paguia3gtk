import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import pandas


class GraphOptions(Gtk.Dialog):
	def __init__(self, dataframe):
		super().__init__()

		self.set_default_size(600, 600)
		self.set_resizable(False)
		self.set_title("Opciones del Grafico")
		self.set_border_width(20)

		self.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK,)

		self.box = self.get_content_area()
		self.box.set_orientation(Gtk.Orientation.VERTICAL)

		features = [x for x in dataframe.columns if x.istitle()]

		self.comboboxtext1 = Gtk.ComboBoxText()
		self.comboboxtext1.append("NULL", "-Seleccione un parametro-")
		self.comboboxtext1.set_active_id("NULL")

		for feature in features:
			self.comboboxtext1.append(f"{feature.upper()}", feature)

		row1 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
		row1.pack_start(Gtk.Label(label="Eje x"), True, True, 0)
		row1.pack_start(self.comboboxtext1, True, True, 0)

		self.comboboxtext2 = Gtk.ComboBoxText()
		self.comboboxtext2.append("NULL", "-Seleccione un parametro-")
		self.comboboxtext2.set_active_id("NULL")
		for feature in features:
			self.comboboxtext2.append(f"{feature.upper()}", feature)

		row2 = Gtk.Box()
		row2.pack_start(Gtk.Label(label="Eje y"), True, True, 0)
		row2.pack_start(self.comboboxtext2, True, True, 0)

		#comboboxtext.connect("changed", self.on_comboboxtext_changed)

		self.box.pack_start(row1, False, True, 0)
		self.box.pack_start(row2, False, True, 0)


	def get_x_axis(self):
		return self.comboboxtext1.get_active_text()


	def get_y_axis(self):
		return self.comboboxtext2.get_active_text()


	def raise_warning(self):
		dialog = Gtk.MessageDialog(
								   parent=self,
								   flags=0,
								   message_type=Gtk.MessageType.WARNING,
								   buttons=Gtk.ButtonsType.CLOSE,
								   text="Entradas inválidas"
								   )
		dialog.format_secondary_text("Seleccione opciones válidas e intente de nuevo")
		response = dialog.run()
		dialog.destroy()
